/* Base Application for Spring Boot */
package com.atlassian.vendorapi

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class VendorapiApplication

fun main(args: Array<String>) {
	runApplication<VendorapiApplication>(*args)
}
