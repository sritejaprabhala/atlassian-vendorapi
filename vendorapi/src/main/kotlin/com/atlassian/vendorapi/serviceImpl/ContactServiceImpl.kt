/* Service Implementation for Account operations */
package com.atlassian.vendorapi.serviceImpl

import com.atlassian.vendorapi.model.Contact
import com.atlassian.vendorapi.repo.AccountRepo
import com.atlassian.vendorapi.repo.ContactRepo
import com.atlassian.vendorapi.service.ContactService
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.bind.annotation.ResponseStatus


@Service
class ContactServiceImpl(private val repo : ContactRepo, private val accRepo : AccountRepo) : ContactService {

    override fun getAllContacts() : List<Contact>?{
        return repo.findAll()
    }
    override fun getContact(ContactId : Long) : Contact?{
        return repo.getOne(ContactId)
    }
    override fun addContact(con : Contact) : Contact{
        val accId = con.account?.id
        if (accId != null)  {
            con.account = accRepo.getOne(accId)
        }
        return  repo.save(con)
    }
    override fun updateContact(con: Contact, id : Long) : Contact{
        con.id = id
        con.account?.id.let{
            val newAccId = it
            val currentCon = repo.getOne(id)
            if (currentCon.account != null && currentCon.account?.id !=  con.account?.id )  {
                var reparent = newAccId?.let { accId -> accRepo.getOne(accId) }
                con.account = reparent
            }
        }
        return  repo.save(con)
    }
    override fun deleteContact(ContactId: Long) : String{
        try {
            repo.deleteById(ContactId) //Deleting record by ID
        }catch (e:IllegalArgumentException){
            return "Delete Failed"
        }
        return "Delete Success"
    }
    override fun deleteContacts(ContactIds: List<Long>) : String{
        //Extra Function - can be added to endpoint whenever needed
        try {
            repo.deleteAll(repo.findAllById(ContactIds))
        }catch (e:IllegalArgumentException){
            return "Delete Failed"
        }
        return "Delete Success"
    }

    fun resourceExists(conId : Long) : Boolean{
        return repo.existsById(conId)
    }
}