/* Service Implementation for Account operations */
package com.atlassian.vendorapi.serviceImpl

import com.atlassian.vendorapi.model.Account
import com.atlassian.vendorapi.model.AccountWithContacts
import com.atlassian.vendorapi.repo.AccountRepo
import com.atlassian.vendorapi.service.AccountService
import org.springframework.stereotype.Service

@Service
class AccountServiceImpl(private val repo : AccountRepo) : AccountService {

    override fun getAllAccounts() : List<Account>?{
        return repo.findAll()
    }
    override fun getAccount(accountId : Long) : Account?{
        return repo.getOne(accountId)
    }
    override fun addAccounts(accRecs : List<Account>) : List<Account>{
        return  repo.saveAll(accRecs)
    }
    override fun updateAccount(acc: Account, id : Long) : Account{
        acc.id = id //Appending or updating ID passed through the URL
        return  repo.save(acc)
    }
    override fun updateAccounts(accRecs: List<Account>) : List<Account>{
        return  repo.saveAll(accRecs)
    }
    override fun deleteAccount(accountId: Long) : String{
        try {
            repo.deleteById(accountId) //Deleting record by ID
        }catch (e:IllegalArgumentException){
            return "Delete Failed"
        }
        return "Delete Success"
    }
    override fun deleteAccounts(accountIds: List<Long>) : String{
        //Extra Function - can be added to endpoint whenever needed
        try {
            repo.deleteAll(repo.findAllById(accountIds))
        }catch (e:IllegalArgumentException){
            return "Delete Failed"
        }
        return "Delete Success"
    }

    override fun getAccountWithContacts(accountId: Long): AccountWithContacts? {
        //Returns a Data Transfer Object (DTO) instead of the Entity - so that we can populate Contact Info as needed
        //Can Enhance in future to add more info/computed fields
        return repo.getOne(accountId).toDto()
    }

    fun resourceExists(accId : Long) : Boolean{
        return repo.existsById(accId)
    }
}