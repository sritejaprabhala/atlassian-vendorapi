/* JPA Repository for Contact Table - Provides basic CRUD operations*/
package com.atlassian.vendorapi.repo

import com.atlassian.vendorapi.model.Contact
import org.springframework.data.jpa.repository.JpaRepository

interface ContactRepo : JpaRepository<Contact, Long> {

}