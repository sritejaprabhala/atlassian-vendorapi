/* JPA Repository for Account Table - Provides basic CRUD operations*/
package com.atlassian.vendorapi.repo

import com.atlassian.vendorapi.model.Account
import org.springframework.data.jpa.repository.JpaRepository

interface AccountRepo : JpaRepository<Account, Long> {
    //For Account with Contacts we can also use a Query in class - but I chose to use DTO instead to keep things simple.
}