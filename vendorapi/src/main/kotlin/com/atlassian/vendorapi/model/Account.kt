/*Entity Class for Account Table - Utilizing H2 in-mem DB*/
package com.atlassian.vendorapi.model

import com.fasterxml.jackson.annotation.JsonIgnore
import java.io.Serializable
import javax.persistence.*

@Entity
@Table(name = "account_data")
data class Account (
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null,

    @Column(name = "company_name")
    var companyName: String = "",

    @Column(name = "address_line1")
    var addressLine1: String = "",

    @Column(name = "address_line2")
    var addressLine2: String? = "",

    @Column(name = "city")
    var city: String? = "",

    @Column(name = "state")
    var state: String? = "",

    @Column(name = "postal_code")
    var postalCode: String? = null,

    @Column(name = "country")
    var country: String? = "",

    @JsonIgnore //Added JSON Ignore to ensure it won't get printed by default
    @OneToMany(mappedBy = "account", cascade = [CascadeType.ALL], fetch = FetchType.LAZY)
    var contacts: List<Contact> = emptyList()
){

    fun toDto() : AccountWithContacts = AccountWithContacts(this)
    //DTO is used to ensure recursion doesn't exist in JSON parsing, and to only use it when we want the contactsRelated node

    fun prepContactsForAccount() : MutableList<ContactWithoutAccount>{
        //Translating List<Contact> to MutableList<ContactWithoutAccount> again to ensure recursion doesn't occur during Jackson json interpretation
        //We can also query for Contacts Data instead of preparing this Wrapper - when data volumes are going to be high. Reducing complexity for MVP
        var conList : MutableList<ContactWithoutAccount> = mutableListOf<ContactWithoutAccount>()
        for(c in this.contacts){
            conList.add(c.toDto())
        }
        return conList
    }
    constructor(): this(-1,"abc", "12", "", "" , "","","")
}

class AccountWithContacts(private val acc:Account) : Serializable {
    //Specs for the Data Transfer Object (DTO)
    val id = acc.id
    val companyName = acc.companyName
    val addressLine1 = acc.addressLine1
    val addressLine2 = acc.addressLine2
    val city = acc.city
    val state = acc.state
    val postalCode = acc.postalCode
    val country = acc.country
    val contactsRelated = acc.prepContactsForAccount()
    //Can Enhance in future to add more info/computed fields of Account/Contact
}