/*Entity Class for Contact Table - Utilizing H2 in-mem DB*/
package com.atlassian.vendorapi.model

import com.fasterxml.jackson.annotation.*
import javax.persistence.*

@Entity
@Table(name = "contact_data")
data class Contact (
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null,

    @Column(name = "name")
    var name: String = "",

    @JsonProperty("account")
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator::class, property = "id") //Ensuring only ID is stamped in JSON instead of whole Account object
    @JsonIdentityReference(alwaysAsId = true)
    @ManyToOne(fetch = FetchType.LAZY, cascade=[CascadeType.ALL])
    @JoinColumn(name = "account", updatable = true )
    var account: Account? = null,

    @Column(name = "email")
    var email: String? = "",

    @Column(name = "address_line1")
    var addressLine1: String = "",

    @Column(name = "address_line2")
    var addressLine2: String? = "",

    @Column(name = "city")
    var city: String? = "",

    @Column(name = "state")
    var state: String? = "",

    @Column(name = "postal_code")
    var postalCode: String? = null,

    @Column(name = "country")
    var country: String? = ""
){
    fun toDto() : ContactWithoutAccount = ContactWithoutAccount(this) //DTO to skip Account Node - for Jackson json
}

class ContactWithoutAccount(private val con:Contact){
    val id = con.id
    val name = con.name
    val addressLine1 = con.addressLine1
    val addressLine2 = con.addressLine2
    val city = con.city
    val state = con.state
    val postalCode = con.postalCode
    val country = con.country
}