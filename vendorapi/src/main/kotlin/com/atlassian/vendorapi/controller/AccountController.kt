/* Base Controller for Handling accounts Endpoint */
package com.atlassian.vendorapi.controller

import ResourceNotFoundException
import com.atlassian.vendorapi.model.Account
import com.atlassian.vendorapi.model.AccountWithContacts
import com.atlassian.vendorapi.serviceImpl.AccountServiceImpl
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/accounts")
@CrossOrigin
class AccountController(private val accSrvImpl: AccountServiceImpl) {

    /* GET Calls Begin */
    @GetMapping("/", "")
    fun getAllAccounts(): List<Account>? = accSrvImpl.getAllAccounts()

    @GetMapping("/{id}", "/{id}/")
    fun getAccount(@PathVariable id : Long): Account? {
        if(accSrvImpl.resourceExists(id)) return accSrvImpl.getAccount(id) else throw ResourceNotFoundException("Account not found with Id :$id")
    }

    @GetMapping("/{id}/contacts", "/{id}/contacts/")
    fun getAccountwithContacts(@PathVariable id : Long): AccountWithContacts?{
        if(accSrvImpl.resourceExists(id)) return accSrvImpl.getAccountWithContacts((id)) else throw ResourceNotFoundException("Account not found with Id :$id")
    }
    /* GET Calls End */

    //PUT Call
    @PutMapping("/", "")
    @ResponseStatus(code = HttpStatus.CREATED)
    fun addAccounts(@RequestBody accRecs: List<Account>) = accSrvImpl.addAccounts(accRecs)

    /* POST Calls Begin */
    @PostMapping("/{id}/", "/{id}")
    fun updateAccount(@RequestBody acc: Account, @PathVariable id : Long) : Account{
        if(accSrvImpl.resourceExists(id)) return accSrvImpl.updateAccount(acc,id) else throw ResourceNotFoundException("Account not found with Id :$id")
    }

    @PostMapping("/", "")
    fun updateAccounts(@RequestBody accRecs: List<Account>) = accSrvImpl.updateAccounts(accRecs)
    /* POST Calls End */

    //Extra - added Delete method just in case if needed
    @DeleteMapping("/{id}", "/id/")
    fun deleteAccount(@PathVariable id: Long): String {
        if(accSrvImpl.resourceExists(id)) return accSrvImpl.deleteAccount(id) else throw ResourceNotFoundException("Account not found with Id :$id")
    }
}