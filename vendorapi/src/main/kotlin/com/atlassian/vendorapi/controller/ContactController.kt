/* Base Controller for Handling accounts Endpoint */
package com.atlassian.vendorapi.controller

import ResourceNotFoundException
import com.atlassian.vendorapi.model.Account
import com.atlassian.vendorapi.model.Contact
import com.atlassian.vendorapi.serviceImpl.ContactServiceImpl
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/contacts")
@CrossOrigin
class ContactController (private val conSrvImpl: ContactServiceImpl) {

    /* GET Calls Begin */
    @GetMapping("/", "")
    fun getAllContacts(): List<Contact>? = conSrvImpl.getAllContacts()

    @GetMapping("/{id}", "/{id}/")
    fun getContact(@PathVariable id: Long): Contact? {
        if(conSrvImpl.resourceExists(id)) return conSrvImpl.getContact(id) else throw ResourceNotFoundException("Contact not found with Id :$id")
    }
    /* GET Calls End */

    //Put Calls
    @PutMapping("/", "")
    @ResponseStatus(code = HttpStatus.CREATED)
    fun addContacts(@RequestBody con: Contact) = conSrvImpl.addContact(con)

    /* POST Calls Begin */
    @PostMapping("/{id}/", "/{id}")
    fun updateContact(@RequestBody con: Contact, @PathVariable id : Long) : Contact{
        if(conSrvImpl.resourceExists(id)) return conSrvImpl.updateContact(con,id) else throw ResourceNotFoundException("Contact not found with Id :$id")
    }
    /* POST Calls End */

    //Extra - added Delete method just in case if needed
    @DeleteMapping("/{id}", "/id/")
    fun deleteContact(@PathVariable id: Long): String = conSrvImpl.deleteContact(id)
}
