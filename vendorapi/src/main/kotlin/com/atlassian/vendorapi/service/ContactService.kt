/* Service Interface for Contact - provides details of all operations */
package com.atlassian.vendorapi.service

import com.atlassian.vendorapi.model.Contact

interface ContactService{

    fun getAllContacts() : List<Contact>?
    fun getContact(ContactId : Long) : Contact?
    fun addContact(con : Contact) : Contact
    fun updateContact(con: Contact, id : Long) : Contact
    fun deleteContact(ContactId: Long) : String
    fun deleteContacts(ContactIds: List<Long>) : String
}