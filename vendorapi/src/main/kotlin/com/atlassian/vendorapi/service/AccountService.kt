/* Service Interface for Account - provides details of all operations */
package com.atlassian.vendorapi.service

import com.atlassian.vendorapi.model.Account
import com.atlassian.vendorapi.model.AccountWithContacts

interface AccountService{

    fun getAllAccounts() : List<Account>?
    fun getAccount(accountId : Long) : Account?
    fun addAccounts(accRecs : List<Account>) : List<Account>
    fun updateAccount(acc: Account, id:Long) : Account
    fun updateAccounts(accRecs: List<Account>) : List<Account>
    fun deleteAccount(accountId: Long) : String
    fun deleteAccounts(accountIds: List<Long>) : String
    fun getAccountWithContacts(accountId: Long) : AccountWithContacts?
}