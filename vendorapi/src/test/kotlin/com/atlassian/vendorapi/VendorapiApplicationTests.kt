package com.atlassian.vendorapi

import com.atlassian.vendorapi.controller.AccountController
import com.atlassian.vendorapi.controller.ContactController
import com.atlassian.vendorapi.model.Account
import com.atlassian.vendorapi.model.Contact
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc


import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType

import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
private class VendorapiApplicationTests(@Autowired val mockMvc: MockMvc) {
	@Autowired
	private val accController: AccountController? = null

	@Autowired
	private val conController: ContactController? = null

	@Test
	@Throws(Exception::class)
	fun contextLoads() {
		assertThat(accController).isNotNull
		assertThat(conController).isNotNull
	}

	@Test
	@Throws(Exception::class)
	fun getAccounts() {
		mockMvc.perform(get("/accounts/")).andExpect(status().isOk).andReturn()
	}

	@Test
	@Throws(Exception::class)
	fun getContacts() {
		mockMvc.perform(get("/contacts/")).andExpect(status().isOk).andReturn()
	}

	@Test
	@Throws(Exception::class)
	fun getAccount() {
		mockMvc.perform(get("/accounts/1")).andExpect(status().isOk).andExpect(jsonPath("$.id").value(1)).andReturn()
	}

	@Test
	@Throws(Exception::class)
	fun getContact() {
		mockMvc.perform(get("/contacts/1")).andExpect(status().isOk).andExpect(jsonPath("$.id").value(1)).andReturn()
	}

	@Test
	@Throws(Exception::class)
	fun putAccount() {
		val accLst : MutableList<Account> = mutableListOf<Account>(Account(companyName = "Acme Industries", addressLine1 = "Street", city = "San Fransisco", country = "USA"))
		mockMvc.perform(put("/accounts/").contentType(MediaType.APPLICATION_JSON).content(jacksonObjectMapper().writeValueAsString(accLst))).andExpect(status().isCreated).andReturn()
	}

	@Test
	@Throws(Exception::class)
	fun putContact() {
		val con = Contact(name = "Acme", addressLine1 = "Street", city = "San Fransisco", country = "USA")
		mockMvc.perform(put("/contacts/").contentType(MediaType.APPLICATION_JSON).content(jacksonObjectMapper().writeValueAsString(con))).andExpect(status().isCreated).andReturn()
	}

	@Test
	@Throws(Exception::class)
	fun updateAccount() {
		val acc = Account(companyName = "Acme Industries", addressLine1 = "Street", city = "San Fransisco", country = "USA")
		mockMvc.perform(post("/accounts/1").contentType(MediaType.APPLICATION_JSON).content(jacksonObjectMapper().writeValueAsString(acc))).andExpect(status().isOk).andReturn()
	}

	@Test
	@Throws(Exception::class)
	fun updateContact() {
		val con = Contact(name = "Acme", addressLine1 = "Street", city = "San Fransisco", country = "USA")
		mockMvc.perform(post("/contacts/1/").contentType(MediaType.APPLICATION_JSON).content(jacksonObjectMapper().writeValueAsString(con))).andExpect(status().isOk).andReturn()
	}
	@Test
	@Throws(Exception::class)
	fun updateAccounts() {
		val accLst : MutableList<Account> = mutableListOf<Account>(Account(id=1, companyName = "Acme Industries", addressLine1 = "Street", city = "San Fransisco", country = "USA"))
		mockMvc.perform(post("/accounts/").contentType(MediaType.APPLICATION_JSON).content(jacksonObjectMapper().writeValueAsString(accLst))).andExpect(status().isOk).andReturn()
	}

	@Test
	@Throws(Exception::class)
	fun getAccountWithContacts() {
		mockMvc.perform(get("/accounts/1/contacts")).andExpect(status().isOk).andExpect(jsonPath("$.contactsRelated").exists()).andReturn()
	}
}
