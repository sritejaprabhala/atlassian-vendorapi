# Getting Started

### Prerequisites
* JDK 11
* Open port 8089 (For Application Server to run). In case of any change - update application.properties 

### Starting the Application
This project uses Gradle to build/run the application. To Start the application, you can do one ofthe following :
* Import into IntelliJ IDE as a Gradle Project, and build VendorApi
* Run "./gradlew bootrun" in the root of the folder. Whenever you are done, please stop using "./gradlew -stop"

### Endpoints available with Sample Data
* GET /accounts/
* GET /accounts/{Id}
* GET /accounts/{Id}/contacts
* PUT /accounts/
    Sample : [{"companyName":"Walgreens","addressLine1":"155 Northgate One","addressLine2":null,"city":"San Rafael","state":"CA","postalCode":"94903","country":"USA"}]
* POST /accounts/
    Sample : [{"companyName":"Walgreens","addressLine1":"155 Northgate One","addressLine2":null,"city":"San Rafael","state":"CA","postalCode":"94903","country":"USA"}]
* POST /accounts/{id}
    Sample : {"companyName":"Walgreens","addressLine1":"155 Northgate One","addressLine2":null,"city":"San Rafael","state":"CA","postalCode":"94903","country":"USA"}

* GET /contacts/
* GET /contacts/{Id}
* PUT /contacts/
    Sample : {"name":"Teja","account":{"id":2},"email":null,"addressLine1":"155 Northgate One","addressLine2":"Apt 1","city":"San Rafael","state":"CA","postalCode":"94903","country":"USA"}
* POST /contacts/{id}
    Sample : {"name":"Teja","account":{"id":2},"email":null,"addressLine1":"155 Northgate One","addressLine2":"Apt 1","city":"San Rafael","state":"CA","postalCode":"94903","country":"USA"}